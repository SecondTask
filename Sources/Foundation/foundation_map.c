#include "foundation_map.h"

struct foundation_map_t *new_foundation_map(foundation_compare fn_c_k, foundation_destroy fn_k_d,
		foundation_destroy fn_v_d) {

	struct foundation_map_t *pMap = (struct foundation_map_t *)malloc(sizeof(struct foundation_map_t));
	if (pMap == (struct foundation_map_t *)0) {
		return (struct foundation_map_t *)0;
	}

	pMap->root = new_foundation_rb(fn_c_k, fn_k_d, fn_v_d);
	if (pMap->root == (struct foundation_rb_t *)0) {
		return (struct foundation_map_t *)0;
	}

	return pMap;
}

foundation_error_t insert_foundation_map(struct foundation_map_t *pMap, void *key, size_t key_size, void *value, size_t value_size) {
	if (pMap == (struct foundation_map_t *)0) {
		return FOUNDATION_MAP_NOT_INITIALIZED;
	}

	return insert_foundation_rb(pMap->root, key, key_size, value, value_size);
}

foundation_bool_t exists_foundation_map(struct foundation_map_t *pMap, void *key) {
	foundation_bool_t found = foundation_false;
	struct foundation_rb_node_t *node;

	if (pMap == (struct foundation_map_t *)0) {
		return foundation_false;
	}

	node = find_foundation_rb(pMap->root, key);
	if (node != (struct foundation_rb_node_t *)0) {
		return foundation_true;
	}
	return found;
}

foundation_error_t
remove_foundation_map(struct foundation_map_t *pMap, void *key) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_rb_node_t *node;
	if (pMap == (struct foundation_map_t *)0) {
		return FOUNDATION_MAP_NOT_INITIALIZED;
	}

	node = remove_foundation_rb(pMap->root, key);
	if (node != (struct foundation_rb_node_t *)0) {
		void *removed_node;
		get_raw_foundation_object(node->key, &removed_node);
		free(removed_node);
		delete_foundation_object(node->key);

		get_raw_foundation_object(node->value, &removed_node);
		free(removed_node);
		delete_foundation_object(node->value);

		free(node);
	}
	return rc;
}

foundation_bool_t find_foundation_map(struct foundation_map_t *pMap, void *key, void **value) {
	struct foundation_rb_node_t *node;

	if (pMap == (struct foundation_map_t *)0) {
		return foundation_false;
	}

	node = find_foundation_rb(pMap->root, key);
	if (node == (struct foundation_rb_node_t *)0) {
		return foundation_false;
	}

	get_raw_foundation_object(node->value, value);

	return foundation_true;

}

foundation_error_t
delete_foundation_map(struct foundation_map_t *x) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	if (x != (struct foundation_map_t *)0) {
		rc = delete_foundation_rb(x->root);
		free(x);
	}
	return rc;
}

static struct foundation_rb_node_t *minimum_foundation_map(struct foundation_map_t *x) {
	return minimum_foundation_rb(x->root, x->root->root);
}

static struct foundation_object_t *get_next_foundation_map(struct foundation_iterator_t *pIterator) {
	if (!pIterator->current_element) {
		pIterator->current_element = minimum_foundation_map(pIterator->container);
	} else {
		struct foundation_map_t *x = (struct foundation_map_t *)pIterator->container;
		pIterator->current_element = tree_successor(x->root, pIterator->current_element);
	}
	if (!pIterator->current_element) {
		return (struct foundation_object_t *)0;
	}

	return ((struct foundation_rb_node_t *)pIterator->current_element)->value;
}

static void *get_value_foundation_map(void *pObject) {
	void *elem;
	get_raw_foundation_object(pObject, &elem);
	return elem;
}

static void replace_value_foundation_map(struct foundation_iterator_t *pIterator, void *elem, size_t elem_size) {
	struct foundation_map_t *pMap = (struct foundation_map_t *)pIterator->container;

	if (pMap->root->destruct_v_fn) {
		void *old_element;
		get_raw_foundation_object(pIterator->current_element, &old_element);
		pMap->root->destruct_v_fn(old_element);
	}
	replace_raw_foundation_object(((struct foundation_rb_node_t *)pIterator->current_element)->value, elem, elem_size);
}


struct foundation_iterator_t *new_iterator_foundation_map(struct foundation_map_t *pMap) {
	struct foundation_iterator_t *itr = (struct foundation_iterator_t *)malloc(sizeof(struct foundation_iterator_t));
	itr->get_next = get_next_foundation_map;
	itr->get_value = get_value_foundation_map;
	itr->replace_value = replace_value_foundation_map;
	itr->container = pMap;
	itr->current_index = 0;
	itr->current_element = (void *)0;
	return itr;
}

void delete_iterator_foundation_map(struct foundation_iterator_t *pItr) {
	free(pItr);
}