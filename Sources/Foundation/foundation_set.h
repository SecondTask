#ifndef _FOUNDATION_SET_H_
#define _FOUNDATION_SET_H_

#include "foundation_library.h"
#include "foundation_rb.h"

struct foundation_set_t {
	struct foundation_rb_t *root;
};

extern struct foundation_set_t *new_foundation_set(foundation_compare fn_c, foundation_destroy fn_d);

extern foundation_error_t insert_foundation_set(struct foundation_set_t *pSet, void *key, size_t key_size);

extern foundation_bool_t exists_foundation_set(struct foundation_set_t *pSet, void *key);

extern foundation_error_t remove_foundation_set(struct foundation_set_t *pSet, void *key);

extern foundation_bool_t find_foundation_set(struct foundation_set_t *pSet, void *key, void *outKey);

extern foundation_error_t delete_foundation_set(struct foundation_set_t *pSet);

extern struct foundation_iterator_t *new_iterator_foundation_set(struct foundation_set_t *pSet);

extern void delete_iterator_foundation_set(struct foundation_iterator_t *pItr);

#endif