#include "foundation_slist.h"

struct foundation_slist_t *new_foundation_slist(
		foundation_compare input_compare_function,
		foundation_destroy input_destroy_function) {
	struct foundation_slist_t *pSlist = malloc(
			sizeof(struct foundation_slist_t)
	);
	pSlist->head = 0;
	pSlist->destruct_function = input_destroy_function;
	pSlist->compare_key_function = input_compare_function;
	pSlist->size = 0;
	return pSlist;
}

void delete_foundation_slist(
		struct foundation_slist_t *input_slist) {
	while (input_slist->size != 0) {
		remove_foundation_slist(input_slist, 0);
	}
	free(input_slist);
}

foundation_error_t push_back_foundation_slist(
		struct foundation_slist_t *input_slist,
		void *element,
		size_t element_size) {

	struct foundation_slist_node_t *current = 0;
	struct foundation_slist_node_t *new_node = malloc(
			sizeof(struct foundation_slist_node_t)
	);

	new_node->element = new_foundation_object(element, element_size);
	if (!new_node->element) {
		return FOUNDATION_SLIST_INSERT_FAILED;
	}
	new_node->next = 0;

	if (input_slist->head == 0) {
		input_slist->head = new_node;
		input_slist->size++;
		return FOUNDATION_ERROR_SUCCESS;
	}
	current = input_slist->head;
	while (current->next != 0) {
		current = current->next;
	}
	current->next = new_node;
	input_slist->size++;

	return FOUNDATION_ERROR_SUCCESS;
}

static void __remove_foundation_list(
		struct foundation_slist_t *input_slist,
		struct foundation_slist_node_t *input_slist_node) {
	void *elem;
	get_raw_foundation_object(
			input_slist_node->element,
			&elem
	);
	if (input_slist->destruct_function) {
		(input_slist->destruct_function)(elem);
		delete_foundation_object(
				input_slist_node->element
		);
	} else {
		free(elem);
		delete_foundation_object(
				input_slist_node->element
		);
	}
	free(input_slist_node);
}

void remove_foundation_slist(struct foundation_slist_t *input_slist, int position) {
	int i = 0;

	struct foundation_slist_node_t *current = input_slist->head;
	struct foundation_slist_node_t *temp = 0;

	if (position > input_slist->size) {return;}

	if (position == 0) {
		input_slist->head = current->next;
		__remove_foundation_list(input_slist, current);
		input_slist->size--;
		return;
	}
	for (i = 1; i < position - 1; i++) {
		current = current->next;
	}

	temp = current->next;
	current->next = current->next->next;
	__remove_foundation_list(input_slist, temp);

	input_slist->size--;
}

foundation_error_t insert_foundation_slist(
		struct foundation_slist_t *input_slist,
		int position,
		void *element,
		size_t element_size) {
	int i = 0;
	struct foundation_slist_node_t *current = input_slist->head;
	struct foundation_slist_node_t *new_node = 0;

	if (position == 1) {
		new_node = malloc(
				sizeof(struct foundation_slist_node_t)
		);
		new_node->element = new_foundation_object(
				element,
				element_size
		);
		if (!new_node->element) {
			free(new_node);
			return FOUNDATION_SLIST_INSERT_FAILED;
		}
		new_node->next = input_slist->head;
		input_slist->head = new_node;
		input_slist->size++;
		return FOUNDATION_ERROR_SUCCESS;
	}

	if (position >= input_slist->size + 1) {
		return push_back_foundation_slist(
				input_slist,
				element,
				element_size
		);
	}

	for (i = 1; i < position - 1; i++) {
		current = current->next;
	}
	new_node = malloc(
			sizeof(struct foundation_slist_node_t)
	);
	new_node->element = new_foundation_object(
			element,
			element_size
	);
	if (!new_node->element) {
		free(new_node);
		return FOUNDATION_SLIST_INSERT_FAILED;
	}

	new_node->next = current->next;
	current->next = new_node;
	input_slist->size++;

	return FOUNDATION_ERROR_SUCCESS;
}

void for_each_foundation_slist(
		struct foundation_slist_t *input_slist,
		void (*input_foreach_function)(void *)) {
	void *element;
	struct foundation_slist_node_t *current = input_slist->head;
	while (current != 0) {
		get_raw_foundation_object(
				current->element,
				&element
		);
		(input_foreach_function)(element);
		free(element);
		current = current->next;
	}
}

foundation_bool_t find_foundation_slist(
		struct foundation_slist_t *input_slist,
		void *find_value,
		void **output_value) {
	struct foundation_slist_node_t *current = input_slist->head;
	while (current != 0) {
		get_raw_foundation_object(
				current->element,
				output_value
		);
		if ((input_slist->compare_key_function)
				(find_value, *output_value) != 0) {
			break;
		}
		free(*output_value);
		current = current->next;
	}
	if (current) {
		return foundation_true;
	}
	return foundation_false;
}


static struct foundation_object_t *get_next_foundation_slist(
		struct foundation_iterator_t *input_iterator) {

	struct foundation_slist_t *pSlist =
			(struct foundation_slist_t *)input_iterator->container;
	if (!input_iterator->current_element) {
		input_iterator->current_element = pSlist->head;
	} else {
		input_iterator->current_element = (
				(struct foundation_slist_node_t *)
						input_iterator->current_element)->next;
	}
	if (!input_iterator->current_element) {
		return 0;
	}

	return ((struct foundation_slist_node_t *)
			input_iterator->current_element)->element;
}

static void *get_value_foundation_slist(
		void *input_object) {
	void *element;
	get_raw_foundation_object(input_object, &element);
	return element;
}

static void replace_value_foundation_slist(
		struct foundation_iterator_t *input_iterator,
		void *element,
		size_t element_size) {
	struct foundation_slist_t *pSlist =
			(struct foundation_slist_t *)
					input_iterator->container;
	struct foundation_object_t *pObj =
			((struct foundation_slist_node_t *)
					input_iterator->current_element)->element;

	if (pSlist->destruct_function) {
		void *old_element;

		get_raw_foundation_object(pObj,
				&old_element
		);
		pSlist->destruct_function(old_element);
	}
	replace_raw_foundation_object(
			pObj,
			element,
			element_size
	);
}

struct foundation_iterator_t *new_iterator_foundation_slist(
		struct foundation_slist_t *pSlist) {
	struct foundation_iterator_t *itr = malloc(
			sizeof(struct foundation_iterator_t)
	);
	itr->get_next = get_next_foundation_slist;
	itr->get_value = get_value_foundation_slist;
	itr->replace_value = replace_value_foundation_slist;
	itr->container = pSlist;
	itr->current_element = NULL;
	itr->current_index = 0;
	return itr;
}

void delete_iterator_foundation_slist(
		struct foundation_iterator_t *input_pointer) {
	free(input_pointer);
}