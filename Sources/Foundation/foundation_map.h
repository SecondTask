#ifndef _FOUNDATION_MAP_H_
#define _FOUNDATION_MAP_H_

#include "foundation_library.h"
#include "foundation_rb.h"

struct foundation_map_t {
	struct foundation_rb_t *root;
};


extern struct foundation_map_t *new_foundation_map(foundation_compare fn_c_k, foundation_destroy fn_k_d, foundation_destroy fn_v_d);

extern foundation_error_t insert_foundation_map(struct foundation_map_t *pMap, void *key, size_t key_size, void *value, size_t value_size);

extern foundation_bool_t exists_foundation_map(struct foundation_map_t *pMap, void *key);

extern foundation_error_t remove_foundation_map(struct foundation_map_t *pMap, void *key);

extern foundation_bool_t find_foundation_map(struct foundation_map_t *pMap, void *key, void **value);

extern foundation_error_t delete_foundation_map(struct foundation_map_t *pMap);

extern struct foundation_iterator_t *new_iterator_foundation_map(struct foundation_map_t *pMap);

extern void delete_iterator_foundation_map(struct foundation_iterator_t *pItr);


#endif