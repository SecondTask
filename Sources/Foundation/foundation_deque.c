#include "foundation_deque.h"

#include <string.h>

#define FOUNDATION_DEQUE_INDEX(x)  ((char *)(pDeq)->pElements + (sizeof(struct clib_object) * (x)))

static foundation_error_t
insert_foundation_deque(struct foundation_deque_t *pDeq, int index, void *elem, size_t elem_size) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_object_t *pObject = new_foundation_object(elem, elem_size);
	if (!pObject) {
		return FOUNDATION_ARRAY_INSERT_FAILED;
	}

	pDeq->pElements[index] = pObject;
	pDeq->no_of_elements++;
	return rc;
}

static struct foundation_deque_t *grow_deque(struct foundation_deque_t *pDeq) {

	pDeq->no_max_elements = pDeq->no_max_elements * 2;
	pDeq->pElements = (struct foundation_object_t **)realloc(pDeq->pElements,
			pDeq->no_max_elements * sizeof(struct foundation_object_t *));
	return pDeq;

}

struct foundation_deque_t *new_foundation_deque(int deq_size, foundation_compare fn_c, foundation_destroy fn_d) {

	struct foundation_deque_t *pDeq = (struct foundation_deque_t *)malloc(sizeof(struct foundation_deque_t));
	if (pDeq == (struct foundation_deque_t *)0) {
		return (struct foundation_deque_t *)0;
	}

	pDeq->no_max_elements = deq_size < 8 ? 8 : deq_size;
	pDeq->pElements = (struct foundation_object_t **)malloc(pDeq->no_max_elements * sizeof(struct foundation_object_t *));

	if (pDeq == (struct foundation_deque_t *)0) {
		return (struct foundation_deque_t *)0;
	}

	pDeq->compare_fn = fn_c;
	pDeq->destruct_fn = fn_d;
	pDeq->head = (int)pDeq->no_max_elements / 2;
	pDeq->tail = pDeq->head + 1;
	pDeq->no_of_elements = 0;

	return pDeq;
}

foundation_error_t
push_back_foundation_deque(struct foundation_deque_t *pDeq, void *elem, size_t elem_size) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}

	if (pDeq->tail == pDeq->no_max_elements) {
		pDeq = grow_deque(pDeq);
	}

	insert_foundation_deque(pDeq, pDeq->tail, elem, elem_size);
	pDeq->tail++;

	return FOUNDATION_ERROR_SUCCESS;
}

foundation_error_t
push_front_foundation_deque(struct foundation_deque_t *pDeq, void *elem, size_t elem_size) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	int to = 0;
	int from = 0;
	int count = 0;

	if (pDeq->head == 0) {
		pDeq = grow_deque(pDeq);
		to = (pDeq->no_max_elements - pDeq->no_of_elements) / 2;
		from = pDeq->head + 1;
		count = pDeq->tail - from + 1;
		memmove (&(pDeq->pElements[to]), &(pDeq->pElements[from]), count * sizeof(struct foundation_object_t *));
		pDeq->head = to - 1;
		pDeq->tail = pDeq->head + count;
	}
	insert_foundation_deque(pDeq, pDeq->head, elem, elem_size);
	pDeq->head--;
	return rc;
}

foundation_error_t
front_foundation_deque(struct foundation_deque_t *pDeq, void *elem) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}
	element_at_foundation_deque(pDeq, pDeq->head + 1, elem);
	return FOUNDATION_ERROR_SUCCESS;
}

foundation_error_t
back_foundation_deque(struct foundation_deque_t *pDeq, void *elem) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}
	element_at_foundation_deque(pDeq, pDeq->tail - 1, elem);
	return FOUNDATION_ERROR_SUCCESS;
}

foundation_error_t
pop_back_foundation_deque(struct foundation_deque_t *pDeq) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}

	if (pDeq->destruct_fn) {
		void *elem;
		if (element_at_foundation_deque(pDeq, pDeq->tail - 1, &elem) == FOUNDATION_ERROR_SUCCESS) {
			pDeq->destruct_fn(elem);
		}
	}
	delete_foundation_object(pDeq->pElements[pDeq->tail - 1]);
	pDeq->tail--;
	pDeq->no_of_elements--;

	return FOUNDATION_ERROR_SUCCESS;

}

foundation_error_t
pop_front_foundation_deque(struct foundation_deque_t *pDeq) {

	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}

	if (pDeq->destruct_fn) {
		void *elem;
		if (element_at_foundation_deque(pDeq, pDeq->head + 1, &elem) == FOUNDATION_ERROR_SUCCESS) {
			pDeq->destruct_fn(elem);
		}
	}
	delete_foundation_object(pDeq->pElements[pDeq->head + 1]);

	pDeq->head++;
	pDeq->no_of_elements--;

	return FOUNDATION_ERROR_SUCCESS;
}

foundation_bool_t
empty_foundation_deque(struct foundation_deque_t *pDeq) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return foundation_true;
	}

	return pDeq->no_of_elements == 0 ? foundation_true : foundation_false;
}

int
size_foundation_deque(struct foundation_deque_t *pDeq) {
	if (pDeq == (struct foundation_deque_t *)0) {
		return foundation_true;
	}

	return pDeq->no_of_elements - 1;
}

foundation_error_t element_at_foundation_deque(struct foundation_deque_t *pDeq, int index, void **elem) {

	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;

	if (!pDeq) {
		return FOUNDATION_DEQUE_NOT_INITIALIZED;
	}

	get_raw_foundation_object(pDeq->pElements[index], elem);
	return rc;
}

foundation_error_t delete_foundation_deque(struct foundation_deque_t *pDeq) {
	int i = 0;

	if (pDeq == (struct foundation_deque_t *)0) {
		return FOUNDATION_ERROR_SUCCESS;
	}

	if (pDeq->destruct_fn) {
		for (i = pDeq->head + 1; i < pDeq->tail; i++) {
			void *elem;
			if (element_at_foundation_deque(pDeq, i, &elem) == FOUNDATION_ERROR_SUCCESS) {
				pDeq->destruct_fn(elem);
			}
		}
	}
	for (i = pDeq->head + 1; i < pDeq->tail; i++) {
		delete_foundation_object(pDeq->pElements[i]);
	}
	free(pDeq->pElements);
	free(pDeq);

	return FOUNDATION_ERROR_SUCCESS;
}

static struct foundation_object_t *get_next_c_deque(struct foundation_iterator_t *pIterator) {
	struct foundation_deque_t *pDeq = (struct foundation_deque_t *)pIterator->container;
	int index = ((struct foundation_iterator_t *)pIterator)->current_index;

	if (index < 0 || index >= pDeq->tail) {
		return (struct foundation_object_t *)0;
	}
	pIterator->current_element = pDeq->pElements[pIterator->current_index++];
	return pIterator->current_element;
}

static void *get_value_c_deque(void *pObject) {
	void *elem;
	get_raw_foundation_object(pObject, &elem);
	return elem;
}

static void replace_value_c_deque(struct foundation_iterator_t *pIterator, void *elem, size_t elem_size) {
	struct foundation_deque_t *pDeq = (struct foundation_deque_t *)pIterator->container;
	if (pDeq->destruct_fn) {
		void *old_element;
		get_raw_foundation_object(pIterator->current_element, &old_element);
		pDeq->destruct_fn(old_element);
	}
	replace_raw_foundation_object(pIterator->current_element, elem, elem_size);
}

struct foundation_iterator_t *new_iterator_foundation_deque(struct foundation_deque_t *pDeq) {
	struct foundation_iterator_t *itr = (struct foundation_iterator_t *)malloc(sizeof(struct foundation_iterator_t));
	itr->get_next = get_next_c_deque;
	itr->get_value = get_value_c_deque;
	itr->replace_value = replace_value_c_deque;
	itr->current_index = pDeq->head + 1;
	itr->container = pDeq;
	return itr;
}

void delete_iterator_foundation_deque(struct foundation_iterator_t *pItr) {
	free(pItr);
}
