#include "foundation_rb.h"

#include <assert.h>

#define rb_sentinel &pTree->sentinel

static void debug_verify_properties(struct foundation_rb_t *);

static void debug_verify_property_1(struct foundation_rb_t *, struct foundation_rb_node_t *);

static void debug_verify_property_2(struct foundation_rb_t *, struct foundation_rb_node_t *);

static int debug_node_color(struct foundation_rb_t *, struct foundation_rb_node_t *n);

static void debug_verify_property_4(struct foundation_rb_t *, struct foundation_rb_node_t *);

static void debug_verify_property_5(struct foundation_rb_t *, struct foundation_rb_node_t *);

static void debug_verify_property_5_helper(struct foundation_rb_t *, struct foundation_rb_node_t *, int, int *);


static void __left_rotate(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	struct foundation_rb_node_t *y;
	y = x->right;
	x->right = y->left;
	if (y->left != rb_sentinel) {
		y->left->parent = x;
	}
	if (y != rb_sentinel) {
		y->parent = x->parent;
	}
	if (x->parent) {
		if (x == x->parent->left) {
			x->parent->left = y;
		} else {
			x->parent->right = y;
		}
	}
	else {
		pTree->root = y;
	}
	y->left = x;
	if (x != rb_sentinel) {
		x->parent = y;
	}
}

static void __right_rotate(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	struct foundation_rb_node_t *y = x->left;
	x->left = y->right;
	if (y->right != rb_sentinel) {
		y->right->parent = x;
	}
	if (y != rb_sentinel) {
		y->parent = x->parent;
	}
	if (x->parent) {
		if (x == x->parent->right) {
			x->parent->right = y;
		} else {
			x->parent->left = y;
		}
	}
	else {
		pTree->root = y;
	}
	y->right = x;
	if (x != rb_sentinel) {
		x->parent = y;
	}
}

struct foundation_rb_t *new_foundation_rb(foundation_compare fn_c, foundation_destroy fn_ed, foundation_destroy fn_vd) {

	struct foundation_rb_t *pTree = (struct foundation_rb_t *)malloc(sizeof(struct foundation_rb_t));
	if (pTree == (struct foundation_rb_t *)0) {
		return (struct foundation_rb_t *)0;
	}

	pTree->compare_fn = fn_c;
	pTree->destruct_k_fn = fn_ed;
	pTree->destruct_v_fn = fn_vd;
	pTree->root = rb_sentinel;
	pTree->sentinel.left = rb_sentinel;
	pTree->sentinel.right = rb_sentinel;
	pTree->sentinel.parent = (struct foundation_rb_node_t *)0;
	pTree->sentinel.color = foundation_black;

	return pTree;
}

static void __rb_insert_fixup(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	while (x != pTree->root && x->parent->color == foundation_red) {
		if (x->parent == x->parent->parent->left) {
			struct foundation_rb_node_t *y = x->parent->parent->right;
			if (y->color == foundation_red) {
				x->parent->color = foundation_black;
				y->color = foundation_black;
				x->parent->parent->color = foundation_red;
				x = x->parent->parent;
			} else {
				if (x == x->parent->right) {
					x = x->parent;
					__left_rotate(pTree, x);
				}
				x->parent->color = foundation_black;
				x->parent->parent->color = foundation_red;
				__right_rotate(pTree, x->parent->parent);
			}
		} else {
			struct foundation_rb_node_t *y = x->parent->parent->left;
			if (y->color == foundation_red) {
				x->parent->color = foundation_black;
				y->color = foundation_black;
				x->parent->parent->color = foundation_red;
				x = x->parent->parent;
			} else {
				if (x == x->parent->left) {
					x = x->parent;
					__right_rotate(pTree, x);
				}
				x->parent->color = foundation_black;
				x->parent->parent->color = foundation_red;
				__left_rotate(pTree, x->parent->parent);
			}
		}
	}
	pTree->root->color = foundation_black;
}

struct foundation_rb_node_t *find_foundation_rb(struct foundation_rb_t *pTree, void *key) {
	struct foundation_rb_node_t *x = pTree->root;

	while (x != rb_sentinel) {
		int c = 0;
		void *cur_key;
		get_raw_foundation_object(x->key, &cur_key);
		c = pTree->compare_fn(key, cur_key);
		free(cur_key);
		if (c == 0) {
			break;
		} else {
			x = c < 0 ? x->left : x->right;
		}
	}
	if (x == rb_sentinel) {
		return (struct foundation_rb_node_t *)0;
	}

	return x;
}

foundation_error_t insert_foundation_rb(struct foundation_rb_t *pTree, void *k, size_t key_size, void *v, size_t value_size) {

	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_rb_node_t *x;
	struct foundation_rb_node_t *y;
	struct foundation_rb_node_t *z;

	x = (struct foundation_rb_node_t *)malloc(sizeof(struct foundation_rb_node_t));
	if (x == (struct foundation_rb_node_t *)0) {
		return FOUNDATION_ERROR_MEMORY;
	}

	x->left = rb_sentinel;
	x->right = rb_sentinel;
	x->color = foundation_red;

	x->key = new_foundation_object(k, key_size);
	if (v) {
		x->value = new_foundation_object(v, value_size);
	} else {
		x->value = (struct foundation_object_t *)0;
	}

	y = pTree->root;
	z = (struct foundation_rb_node_t *)0;

	while (y != rb_sentinel) {
		int c = 0;
		void *cur_key;
		void *new_key;

		get_raw_foundation_object(y->key, &cur_key);
		get_raw_foundation_object(x->key, &new_key);

		c = (pTree->compare_fn)(new_key, cur_key);
		free(cur_key);
		free(new_key);
		if (c == 0) {
			/* TODO : Delete node here */
			return FOUNDATION_RBTREE_KEY_DUPLICATE;
		}
		z = y;
		if (c < 0) {
			y = y->left;
		} else {
			y = y->right;
		}
	}
	x->parent = z;
	if (z) {
		int c = 0;
		void *cur_key;
		void *new_key;
		get_raw_foundation_object(z->key, &cur_key);
		get_raw_foundation_object(x->key, &new_key);

		c = pTree->compare_fn(new_key, cur_key);
		free(cur_key);
		free(new_key);
		if (c < 0) {
			z->left = x;
		} else {
			z->right = x;
		}
	}
	else {
		pTree->root = x;
	}

	__rb_insert_fixup(pTree, x);

	debug_verify_properties(pTree);
	return rc;
}

static void __rb_remove_fixup(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	while (x != pTree->root && x->color == foundation_black) {
		if (x == x->parent->left) {
			struct foundation_rb_node_t *w = x->parent->right;
			if (w->color == foundation_red) {
				w->color = foundation_black;
				x->parent->color = foundation_red;
				__left_rotate(pTree, x->parent);
				w = x->parent->right;
			}
			if (w->left->color == foundation_black && w->right->color == foundation_black) {
				w->color = foundation_red;
				x = x->parent;
			} else {
				if (w->right->color == foundation_black) {
					w->left->color = foundation_black;
					w->color = foundation_red;
					__right_rotate(pTree, w);
					w = x->parent->right;
				}
				w->color = x->parent->color;
				x->parent->color = foundation_black;
				w->right->color = foundation_black;
				__left_rotate(pTree, x->parent);
				x = pTree->root;
			}
		} else {
			struct foundation_rb_node_t *w = x->parent->left;
			if (w->color == foundation_red) {
				w->color = foundation_black;
				x->parent->color = foundation_red;
				__right_rotate(pTree, x->parent);
				w = x->parent->left;
			}
			if (w->right->color == foundation_black && w->left->color == foundation_black) {
				w->color = foundation_red;
				x = x->parent;
			} else {
				if (w->left->color == foundation_black) {
					w->right->color = foundation_black;
					w->color = foundation_red;
					__left_rotate(pTree, w);
					w = x->parent->left;
				}
				w->color = x->parent->color;
				x->parent->color = foundation_black;
				w->left->color = foundation_black;
				__right_rotate(pTree, x->parent);
				x = pTree->root;
			}
		}
	}
	x->color = foundation_black;
}

static struct foundation_rb_node_t *__remove_foundation_rb(struct foundation_rb_t *pTree, struct foundation_rb_node_t *z) {
	struct foundation_rb_node_t *x = (struct foundation_rb_node_t *)0;
	struct foundation_rb_node_t *y = (struct foundation_rb_node_t *)0;

	if (z->left == rb_sentinel || z->right == rb_sentinel) {
		y = z;
	} else {
		y = z->right;
		while (y->left != rb_sentinel) {
			y = y->left;
		}
	}
	if (y->left != rb_sentinel) {
		x = y->left;
	} else {
		x = y->right;
	}

	x->parent = y->parent;
	if (y->parent) {
		if (y == y->parent->left) {
			y->parent->left = x;
		} else {
			y->parent->right = x;
		}
	}
	else {
		pTree->root = x;
	}
	if (y != z) {
		struct foundation_object_t *tmp;
		tmp = z->key;
		z->key = y->key;
		y->key = tmp;

		tmp = z->value;
		z->value = y->value;
		y->value = tmp;
	}
	if (y->color == foundation_black) {
		__rb_remove_fixup(pTree, x);
	}

	debug_verify_properties(pTree);
	return y;
}

struct foundation_rb_node_t * remove_foundation_rb(struct foundation_rb_t *pTree, void *key) {
	struct foundation_rb_node_t *z = (struct foundation_rb_node_t *)0;

	z = pTree->root;
	while (z != rb_sentinel) {
		int c = 0;
		void *cur_key;
		get_raw_foundation_object(z->key, &cur_key);
		c = pTree->compare_fn(key, cur_key);
		free(cur_key);
		if (c == 0) {
			break;
		}
		else {
			z = (c < 0) ? z->left : z->right;
		}
	}
	if (z == rb_sentinel) {
		return (struct foundation_rb_node_t *)0;
	}
	return __remove_foundation_rb(pTree, z);
}

static void __delete_foundation_rb_node(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {

	void *key;
	void *value;

	if (pTree->destruct_k_fn) {
		get_raw_foundation_object(x->key, &key);
		pTree->destruct_k_fn(key);
	}
	delete_foundation_object(x->key);

	if (x->value) {
		if (pTree->destruct_v_fn) {
			get_raw_foundation_object(x->value, &value);
			pTree->destruct_v_fn(value);
		}
		delete_foundation_object(x->value);
	}
}

foundation_error_t delete_foundation_rb(struct foundation_rb_t *pTree) {

	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_rb_node_t *z = pTree->root;

	while (z != rb_sentinel) {
		if (z->left != rb_sentinel) {
			z = z->left;
		} else if (z->right != rb_sentinel) {
			z = z->right;
		} else {
			__delete_foundation_rb_node(pTree, z);
			if (z->parent) {
				z = z->parent;
				if (z->left != rb_sentinel) {
					free(z->left);
					z->left = rb_sentinel;
				} else if (z->right != rb_sentinel) {
					free(z->right);
					z->right = rb_sentinel;
				}
			} else {
				free(z);
				z = rb_sentinel;
			}
		}
	}
	free(pTree);
	return rc;
}

struct foundation_rb_node_t *minimum_foundation_rb(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	while (x->left != rb_sentinel) {
		x = x->left;
	}
	return x;
}

struct foundation_rb_node_t *maximum_foundation_rb(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	while (x->right != rb_sentinel) {
		x = x->right;
	}
	return x;
}


foundation_bool_t empty_foundation_rb(struct foundation_rb_t *pTree) {
	if (pTree->root != rb_sentinel) {
		return foundation_true;
	}
	return foundation_false;
}

struct foundation_rb_node_t *tree_successor(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x) {
	struct foundation_rb_node_t *y = (struct foundation_rb_node_t *)0;
	if (x->right != rb_sentinel) {
		return minimum_foundation_rb(pTree, x->right);
	}

	if (x == maximum_foundation_rb(pTree, pTree->root)) {
		return (struct foundation_rb_node_t *)0;
	}

	y = x->parent;
	while (y != rb_sentinel && x == y->right) {
		x = y;
		y = y->parent;
	}
	return y;
}

/*struct clib_rb_node*get_next_c_rb(struct foundation_rb_t* pTree, struct clib_rb_node**current, struct clib_rb_node**pre) {
    struct clib_rb_node* prev_current;
    while((*current) != rb_sentinel) {
        if((*current)->left == rb_sentinel){
            prev_current = (*current);
            (*current) = (*current)->right;
            return prev_current->raw_data.key;			
        } else {
            (*pre) = (*current)->left;
            while((*pre)->right != rb_sentinel && (*pre)->right != (*current))
                (*pre) = (*pre)->right; 
            if((*pre)->right == rb_sentinel) {
                (*pre)->right = (*current);
                (*current) = (*current)->left;
            } else {
                (*pre)->right = rb_sentinel;
                prev_current = (*current);
                (*current) = (*current)->right;
                return prev_current->raw_data.key;				
            }
        } 
    } 
    return (struct clib_rb_node*)0 ;
}*/

void debug_verify_properties(struct foundation_rb_t *t) {
	debug_verify_property_1(t, t->root);
	debug_verify_property_2(t, t->root);
	debug_verify_property_4(t, t->root);
	debug_verify_property_5(t, t->root);
}

void debug_verify_property_1(struct foundation_rb_t *pTree, struct foundation_rb_node_t *n) {
	assert(debug_node_color(pTree, n) == foundation_red || debug_node_color(pTree, n) == foundation_black);
	if (n == rb_sentinel) {return;}
	debug_verify_property_1(pTree, n->left);
	debug_verify_property_1(pTree, n->right);
}

void debug_verify_property_2(struct foundation_rb_t *pTree, struct foundation_rb_node_t *root) {
	assert(debug_node_color(pTree, root) == foundation_black);
}

int debug_node_color(struct foundation_rb_t *pTree, struct foundation_rb_node_t *n) {
	return n == rb_sentinel ? foundation_black : n->color;
}

void debug_verify_property_4(struct foundation_rb_t *pTree, struct foundation_rb_node_t *n) {
	if (debug_node_color(pTree, n) == foundation_red) {
		assert (debug_node_color(pTree, n->left) == foundation_black);
		assert (debug_node_color(pTree, n->right) == foundation_black);
		assert (debug_node_color(pTree, n->parent) == foundation_black);
	}
	if (n == rb_sentinel) {return;}
	debug_verify_property_4(pTree, n->left);
	debug_verify_property_4(pTree, n->right);
}

void debug_verify_property_5(struct foundation_rb_t *pTree, struct foundation_rb_node_t *root) {
	int black_count_path = -1;
	debug_verify_property_5_helper(pTree, root, 0, &black_count_path);
}

void debug_verify_property_5_helper(struct foundation_rb_t *pTree, struct foundation_rb_node_t *n, int black_count, int *path_black_count) {
	if (debug_node_color(pTree, n) == foundation_black) {
		black_count++;
	}
	if (n == rb_sentinel) {
		if (*path_black_count == -1) {
			*path_black_count = black_count;
		} else {
			assert (black_count == *path_black_count);
		}
		return;
	}
	debug_verify_property_5_helper(pTree, n->left, black_count, path_black_count);
	debug_verify_property_5_helper(pTree, n->right, black_count, path_black_count);
}