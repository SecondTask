#include "foundation_set.h"

struct foundation_set_t *new_foundation_set(foundation_compare fn_c, foundation_destroy fn_d) {

	struct foundation_set_t *pSet = (struct foundation_set_t *)malloc(sizeof(struct foundation_set_t));
	if (pSet == (struct foundation_set_t *)0) {
		return (struct foundation_set_t *)0;
	}

	pSet->root = new_foundation_rb(fn_c, fn_d, (void *)0);
	if (pSet->root == (struct foundation_rb_t *)0) {
		return (struct foundation_set_t *)0;
	}

	return pSet;
}

foundation_error_t insert_foundation_set(struct foundation_set_t *pSet, void *key, size_t key_size) {
	if (pSet == (struct foundation_set_t *)0) {
		return FOUNDATION_SET_NOT_INITIALIZED;
	}

	return insert_foundation_rb(pSet->root, key, key_size, (void *)0, 0);
}

foundation_bool_t exists_foundation_set(struct foundation_set_t *pSet, void *key) {
	foundation_bool_t found = foundation_false;
	struct foundation_rb_node_t *node;

	if (pSet == (struct foundation_set_t *)0) {
		return foundation_false;
	}

	node = find_foundation_rb(pSet->root, key);
	if (node != (struct foundation_rb_node_t *)0) {
		return foundation_true;
	}
	return found;
}

foundation_error_t
remove_foundation_set(struct foundation_set_t *pSet, void *key) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_rb_node_t *node;
	if (pSet == (struct foundation_set_t *)0) {
		return FOUNDATION_SET_NOT_INITIALIZED;
	}

	node = remove_foundation_rb(pSet->root, key);
	if (node != (struct foundation_rb_node_t *)0) {
		/*free ( node->raw_data.key);
		free ( node );*/
	}
	return rc;
}

foundation_bool_t find_foundation_set(struct foundation_set_t *pSet, void *key, void *outKey) {
	struct foundation_rb_node_t *node;

	if (pSet == (struct foundation_set_t *)0) {
		return foundation_false;
	}

	node = find_foundation_rb(pSet->root, key);
	if (node == (struct foundation_rb_node_t *)0) {
		return foundation_false;
	}

	get_raw_foundation_object(node->key, outKey);

	return foundation_true;

}

foundation_error_t delete_foundation_set(struct foundation_set_t *x) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	if (x != (struct foundation_set_t *)0) {
		rc = delete_foundation_rb(x->root);
		free(x);
	}
	return rc;
}

static struct foundation_rb_node_t *minimum_foundation_set(struct foundation_set_t *x) {
	return minimum_foundation_rb(x->root, x->root->root);
}

static struct foundation_object_t *get_next_foundation_set(struct foundation_iterator_t *pIterator) {
	if (!pIterator->current_element) {
		pIterator->current_element = minimum_foundation_set(pIterator->container);
	} else {
		struct foundation_set_t *x = (struct foundation_set_t *)pIterator->container;
		pIterator->current_element = tree_successor(x->root, pIterator->current_element);
	}
	if (!pIterator->current_element) {
		return (struct foundation_object_t *)0;
	}

	return ((struct foundation_rb_node_t *)pIterator->current_element)->key;
}

static void *get_value_foundation_set(void *pObject) {
	void *elem;
	get_raw_foundation_object(pObject, &elem);
	return elem;
}

struct foundation_iterator_t *new_iterator_foundation_set(struct foundation_set_t *pSet) {
	struct foundation_iterator_t *itr = (struct foundation_iterator_t *)malloc(sizeof(struct foundation_iterator_t));
	itr->get_next = get_next_foundation_set;
	itr->get_value = get_value_foundation_set;
	itr->container = pSet;
	itr->current_index = 0;
	itr->current_element = (void *)0;
	return itr;
}

void delete_iterator_foundation_set(struct foundation_iterator_t *pItr) {
	free(pItr);
}
