#include "foundation_algorithms.h"

void foundation_for_each(struct foundation_iterator_t *pIterator, void (*fn)(void *)) {
	struct foundation_object_t *pElement;

	pElement = pIterator->get_next(pIterator);
	while (pElement) {
		void *value = pIterator->get_value(pElement);
		(fn)(value);
		free(value);
		pElement = pIterator->get_next(pIterator);
	}
}

