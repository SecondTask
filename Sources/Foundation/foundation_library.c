#include <string.h>
#include <stdlib.h>

#include "foundation_library.h"

void foundation_copy(void *destination, void *source, size_t size) {
	memcpy ((char *)destination, source, size);
}

void foundation_get(void *destination, void *source, size_t size) {
	memcpy (destination, (char *)source, size);
}

struct foundation_object_t *new_foundation_object(void *input_object, size_t object_size) {
	struct foundation_object_t *tmp = malloc(sizeof(struct foundation_object_t));
	if (!tmp) {
		return NULL;
	}
	tmp->size = object_size;
	tmp->raw_data = malloc(object_size);
	if (!tmp->raw_data) {
		free(tmp);
		return NULL;
	}
	memcpy (tmp->raw_data, input_object, object_size);
	return tmp;
}

foundation_error_t get_raw_foundation_object(struct foundation_object_t *input_object, void **output_element) {
	*output_element = malloc(input_object->size);
	if (!*output_element) {
		return FOUNDATION_ELEMENT_RETURN_ERROR;
	}
	memcpy(*output_element, input_object->raw_data, input_object->size);

	return FOUNDATION_ERROR_SUCCESS;
}

void replace_raw_foundation_object(struct foundation_object_t *current_object, void *element, size_t element_size) {
	free(current_object->raw_data);
	current_object->raw_data = malloc(element_size);
	memcpy (current_object->raw_data, element, element_size);
}


void delete_foundation_object(struct foundation_object_t *input_object) {
	if (input_object) {
		free(input_object->raw_data);
		free(input_object);
	}
}

char *foundation_strdup(char *ptr) {
#ifdef WIN32
        return _strdup (ptr);
    #else
	return strdup(ptr);
#endif
}