#ifndef _FOUNDATION_ARRAY_H_
#define _FOUNDATION_ARRAY_H_

#include <string.h>

#include "foundation_library.h"

struct foundation_array_t {
	int capacity;
	/* Number of maximum elements array can hold without reallocation */
	int amount;
	/* Number of current elements in the array */
	struct foundation_object_t **elements;
	/* actual storage area */

	foundation_compare compare_function;
	/* Compare function pointer*/
	foundation_destroy destruct_function; /* Destructor function pointer*/
};

extern struct foundation_array_t *new_foundation_array(int init_size, foundation_compare input_compare_function, foundation_destroy input_destroy_function);

extern foundation_error_t push_back_foundation_array(struct foundation_array_t *input_array, void *element, size_t element_size);

extern foundation_error_t element_at_foundation_array(struct foundation_array_t *input_array, int position, void **element);

extern foundation_error_t insert_at_foundation_array(struct foundation_array_t *input_array, int index, void *element, size_t element_size);

extern int size_foundation_array(struct foundation_array_t *input_array);

extern int capacity_foundation_array(struct foundation_array_t *input_array);

extern foundation_bool_t empty_foundation_array(struct foundation_array_t *input_array);

extern foundation_error_t reserve_foundation_array(struct foundation_array_t *input_array, int position);

extern foundation_error_t front_foundation_array(struct foundation_array_t *input_array, void *element);

extern foundation_error_t back_foundation_array(struct foundation_array_t *input_array, void *element);

extern foundation_error_t remove_from_foundation_array(struct foundation_array_t *input_array, int position);

extern foundation_error_t delete_foundation_array(struct foundation_array_t *input_array);

extern struct foundation_iterator_t *new_iterator_foundation_array(struct foundation_array_t *input_array);

extern void delete_iterator_foundation_array(struct foundation_iterator_t *input_iterator);

#endif