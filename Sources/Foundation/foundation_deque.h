#ifndef _FOUNDATION_DEQUE_H_
#define _FOUNDATION_DEQUE_H_

#include "foundation_library.h"

struct foundation_deque_t {
	struct foundation_object_t **pElements;
	int no_max_elements;
	int head;
	int tail;
	int no_of_elements;
	foundation_compare compare_fn;
	foundation_destroy destruct_fn;
} foundation_deque;

extern struct foundation_deque_t *new_foundation_deque(int deq_size, foundation_compare fn_c, foundation_destroy fn_d);

extern foundation_error_t push_back_foundation_deque(struct foundation_deque_t *pDeq, void *elem, size_t elem_size);

extern foundation_error_t push_front_foundation_deque(struct foundation_deque_t *pDeq, void *elem, size_t elem_size);

extern foundation_error_t front_foundation_deque(struct foundation_deque_t *pDeq, void *);

extern foundation_error_t back_foundation_deque(struct foundation_deque_t *pDeq, void *);

extern foundation_error_t pop_back_foundation_deque(struct foundation_deque_t *pDeq);

extern foundation_error_t pop_front_foundation_deque(struct foundation_deque_t *pDeq);

extern foundation_bool_t empty_foundation_deque(struct foundation_deque_t *pDeq);

extern int size_foundation_deque(struct foundation_deque_t *pDeq);

extern foundation_error_t delete_foundation_deque(struct foundation_deque_t *pDeq);

extern foundation_error_t element_at_foundation_deque(struct foundation_deque_t *pDeq, int index, void **elem);

extern struct foundation_iterator_t *new_iterator_foundation_deque(struct foundation_deque_t *pDeq);

extern void delete_iterator_foundation_deque(struct foundation_iterator_t *pItr);

#endif