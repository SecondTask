#ifndef _FOUNDATION_SLIST_H_
#define _FOUNDATION_SLIST_H_

#include "foundation_library.h"

struct foundation_slist_node_t {
	struct foundation_object_t *element;
	struct foundation_slist_node_t *next;
};


struct foundation_slist_t {
	struct foundation_slist_node_t *head;
	foundation_destroy destruct_function;
	foundation_compare compare_key_function;
	int size;
};


extern struct foundation_slist_t *new_foundation_slist(
		foundation_compare input_compare_function,
		foundation_destroy input_destroy_function
);

extern void delete_foundation_slist(
		struct foundation_slist_t *input_slist
);

extern foundation_error_t insert_foundation_slist(
		struct foundation_slist_t *input_slist,
		int position,
		void *element,
		size_t element_size
);

extern foundation_error_t push_back_foundation_slist(
		struct foundation_slist_t *input_slist,
		void *element,
		size_t element_size
);

extern void remove_foundation_slist(
		struct foundation_slist_t *input_slist,
		int position
);

extern void for_each_foundation_slist(
		struct foundation_slist_t *input_slist,
		void (*input_foreach_function)(void *)
);

extern foundation_bool_t find_foundation_slist(
		struct foundation_slist_t *input_slist,
		void *find_value,
		void **output_value
);

extern struct foundation_iterator_t *new_iterator_foundation_slist(
		struct foundation_slist_t *input_slist
);

extern void delete_iterator_foundation_slist(
		struct foundation_iterator_t *input_pointer
);

#endif