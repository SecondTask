#ifndef _FOUNDATION_RB_H_
#define _FOUNDATION_RB_H_

#include "foundation_library.h"

struct foundation_rb_node_t {
	struct foundation_rb_node_t *left;
	struct foundation_rb_node_t *right;
	struct foundation_rb_node_t *parent;
	int color;
	struct foundation_object_t *key;
	struct foundation_object_t *value;
};

struct foundation_rb_t {
	struct foundation_rb_node_t *root;
	struct foundation_rb_node_t sentinel;
	foundation_destroy destruct_k_fn;
	foundation_destroy destruct_v_fn;
	foundation_compare compare_fn;
};

extern struct foundation_rb_t *new_foundation_rb(foundation_compare fn_c, foundation_destroy fn_ed, foundation_destroy fn_vd);

extern foundation_error_t insert_foundation_rb(struct foundation_rb_t *pTree, void *key, size_t key_size, void *value, size_t value_size);

extern struct foundation_rb_node_t *find_foundation_rb(struct foundation_rb_t *pTree, void *key);

extern struct foundation_rb_node_t *remove_foundation_rb(struct foundation_rb_t *pTree, void *key);

extern foundation_error_t delete_foundation_rb(struct foundation_rb_t *pTree);

extern foundation_bool_t empty_foundation_rb(struct foundation_rb_t *pTree);

extern struct foundation_rb_node_t *minimum_foundation_rb(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x);

extern struct foundation_rb_node_t *tree_successor(struct foundation_rb_t *pTree, struct foundation_rb_node_t *x);

#endif