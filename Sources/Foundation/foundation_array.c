#include "foundation_array.h"

static struct foundation_array_t *array_check_and_grow(struct foundation_array_t *pArray) {
	if (pArray->amount >= pArray->capacity) {
		pArray->capacity = (unsigned int)(1.5 * pArray->capacity);
		pArray->elements = realloc(pArray->elements, pArray->capacity * sizeof(struct foundation_object_t *));
	}
	return pArray;
}

struct foundation_array_t *new_foundation_array(int array_size, foundation_compare input_compare_function, foundation_destroy input_destroy_function) {

	struct foundation_array_t *pArray = malloc(sizeof(struct foundation_array_t));
	if (!pArray) {
		return 0;
	}

	pArray->capacity = array_size < 8 ? 8 : array_size;
	pArray->elements = malloc(pArray->capacity * sizeof(struct foundation_object_t *));
	if (!pArray->elements) {
		free(pArray);
		return 0;
	}
	pArray->compare_function = input_compare_function;
	pArray->destruct_function = input_destroy_function;
	pArray->amount = 0;

	return pArray;
}

static foundation_error_t insert_foundation_array(struct foundation_array_t *pArray, int index, void *elem, size_t elem_size) {

	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	struct foundation_object_t *pObject = new_foundation_object(elem, elem_size);
	if (!pObject) {
		return FOUNDATION_ARRAY_INSERT_FAILED;
	}

	pArray->elements[index] = pObject;
	pArray->amount++;
	return rc;
}

foundation_error_t push_back_foundation_array(struct foundation_array_t *input_array, void *element, size_t element_size) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;

	if (!input_array) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	array_check_and_grow(input_array);

	rc = insert_foundation_array(input_array, input_array->amount, element, element_size);

	return rc;
}

foundation_error_t element_at_foundation_array(struct foundation_array_t *input_array, int position, void **element) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;

	if (!input_array) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	if (position < 0 || position > input_array->capacity) {
		return FOUNDATION_ARRAY_INDEX_OUT_OF_BOUND;
	}

	get_raw_foundation_object(input_array->elements[position], element);
	return rc;
}

int size_foundation_array(struct foundation_array_t *input_array) {
	if (input_array == 0) {
		return 0;
	}
	return input_array->amount - 1;
}

int capacity_foundation_array(struct foundation_array_t *input_array) {
	if (input_array == 0) {
		return 0;
	}
	return input_array->capacity;
}

foundation_bool_t empty_foundation_array(struct foundation_array_t *input_array) {
	if (input_array == 0) {
		return 0;
	}
	return input_array->amount == 0 ? foundation_true : foundation_false;
}

foundation_error_t reserve_foundation_array(struct foundation_array_t *input_array, int position) {
	if (input_array == 0) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	if (position <= input_array->capacity) {
		return FOUNDATION_ERROR_SUCCESS;
	}

	array_check_and_grow(input_array);
	return FOUNDATION_ERROR_SUCCESS;

}

foundation_error_t front_foundation_array(struct foundation_array_t *input_array, void *element) {
	return element_at_foundation_array(input_array, 0, element);
}

foundation_error_t back_foundation_array(struct foundation_array_t *input_array, void *element) {
	return element_at_foundation_array(input_array, input_array->amount - 1, element);
}

foundation_error_t insert_at_foundation_array(struct foundation_array_t *input_array, int index, void *element, size_t element_size) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	if (!input_array) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	if (index < 0 || index > input_array->capacity) {
		return FOUNDATION_ARRAY_INDEX_OUT_OF_BOUND;
	}

	array_check_and_grow(input_array);

	memmove (&(input_array->elements[index + 1]), &input_array->elements[index], (input_array->amount - index) * sizeof(struct foundation_object_t *));

	rc = insert_foundation_array(input_array, index, element, element_size);

	return rc;
}

foundation_error_t remove_from_foundation_array(struct foundation_array_t *input_array, int index) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;

	if (!input_array) {
		return rc;
	}
	if (index < 0 || index > input_array->capacity) {
		return FOUNDATION_ARRAY_INDEX_OUT_OF_BOUND;
	}

	if (input_array->destruct_function) {
		void *elem;
		if (FOUNDATION_ERROR_SUCCESS == element_at_foundation_array(input_array, index, &elem)) {
			input_array->destruct_function(elem);
		}
	}
	delete_foundation_object(input_array->elements[index]);

	memmove(&(input_array->elements[index]), &input_array->elements[index + 1], (input_array->amount - index) * sizeof(struct foundation_object_t *));
	input_array->amount--;

	return rc;
}

foundation_error_t delete_foundation_array(
		struct foundation_array_t *input_array) {
	foundation_error_t rc = FOUNDATION_ERROR_SUCCESS;
	int i = 0;

	if (input_array == 0) {
		return rc;
	}

	if (input_array->destruct_function) {
		for (i = 0; i < input_array->amount; i++) {
			void *elem;
			if (FOUNDATION_ERROR_SUCCESS == element_at_foundation_array(input_array, i, &elem)) {
				input_array->destruct_function(elem);
			}
		}
	}

	for (i = 0; i < input_array->amount; i++) {
		delete_foundation_object(input_array->elements[i]);
	}

	free(input_array->elements);
	free(input_array);
	return rc;
}

static struct foundation_object_t *get_next_foundation_array(struct foundation_iterator_t *input_iterator) {

	struct foundation_array_t *pArray = (struct foundation_array_t *)input_iterator->container;

	if (input_iterator->current_index > size_foundation_array(pArray)) {
		return 0;
	}

	input_iterator->current_element = pArray->elements[input_iterator->current_index++];
	return input_iterator->current_element;
}

static void *get_value_foundation_array(void *input_object) {
	void *elem;
	get_raw_foundation_object(input_object, &elem);
	return elem;
}

static void replace_value_foundation_array(struct foundation_iterator_t *input_iterator, void *element, size_t element_size) {
	struct foundation_array_t *pArray = (struct foundation_array_t *)input_iterator->container;

	if (pArray->destruct_function) {
		void *old_element;
		get_raw_foundation_object(input_iterator->current_element, &old_element);
		pArray->destruct_function(old_element);
	}
	replace_raw_foundation_object(input_iterator->current_element, element, element_size);
}

struct foundation_iterator_t *
new_iterator_foundation_array(struct foundation_array_t *input_array) {
	struct foundation_iterator_t *itr = malloc(sizeof(struct foundation_iterator_t));
	itr->get_next = get_next_foundation_array;
	itr->get_value = get_value_foundation_array;
	itr->replace_value = replace_value_foundation_array;
	itr->container = input_array;
	itr->current_index = 0;
	return itr;
}

void delete_iterator_foundation_array(struct foundation_iterator_t *input_iterator) {
	free(input_iterator);
}