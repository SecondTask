#ifndef _FOUNDATION_LIBRARY_H_
#define _FOUNDATION_LIBRARY_H_

#include <stdlib.h>

#include "foundation_errors.h"

typedef void (*foundation_destroy)(void *);

typedef int  (*foundation_compare)(void *, void *);

typedef void (*foundation_traversal)(void *);

typedef int foundation_error_t;
typedef int foundation_bool_t;

#define foundation_black           0
#define foundation_red             1
#define foundation_true            1
#define foundation_false           0

struct foundation_object_t {
	void *raw_data;
	size_t size;
};


struct foundation_iterator_t {
	struct foundation_object_t *(*get_next)(struct foundation_iterator_t *);

	void (*replace_value)(struct foundation_iterator_t *, void *, size_t);

	void *(*get_value)(void *);

	void *container;
	int current_index;
	void *current_element;
};

extern void foundation_copy(void *destination, void *source, size_t size);

extern void foundation_get(void *destination, void *source, size_t size);

extern char *foundation_strdup(char *ptr);

extern struct foundation_object_t *new_foundation_object(void *input_object, size_t object_size);

extern foundation_error_t get_raw_foundation_object(struct foundation_object_t *input_object, void **output_element);

extern void delete_foundation_object(struct foundation_object_t *input_object);

extern void replace_raw_foundation_object(struct foundation_object_t *current_object, void *element, size_t element_size);

#endif
