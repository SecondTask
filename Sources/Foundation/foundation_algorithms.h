#ifndef _FOUNDATION_ALGORITHMS_
#define _FOUNDATION_ALGORITHMS_

#include "foundation_library.h"

extern void foundation_for_each(struct foundation_iterator_t *pIterator, void (*fn)(void *));

#endif