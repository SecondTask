#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#include "foundation_library.h"
#include "foundation_array.h"
#include "foundation_slist.h"

#define WORDS_DELIMITER ','
#define LINE_FINISH '.'
#define MAX_UPPER_LETTERS_COUNT 6
#define MIN_UPPER_LETTERS_COUNT 1

static int comparison_function(void *first, void *second) {
	return *(int *)first == *(int *)second;
}

int foundation_string_comparison(struct foundation_object_t const *first, struct foundation_object_t const *second) {
	return strcmp((*(struct foundation_object_t **)first)->raw_data, (*(struct foundation_object_t **)second)->raw_data);
}

foundation_error_t get_input_foundation_array(struct foundation_array_t *output_array) {
	foundation_error_t return_code = FOUNDATION_ERROR_SUCCESS;

	if (!output_array) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	struct foundation_array_t *return_array = new_foundation_array(0, comparison_function, free);

	assert(empty_foundation_array(return_array) == foundation_true);

	char inputCharacter = 0;
	while ((inputCharacter = (char)getchar()) && inputCharacter != LINE_FINISH) {
		push_back_foundation_array(return_array, &inputCharacter, sizeof(char));
	}

	push_back_foundation_array(return_array, &inputCharacter, sizeof(char));

	assert(empty_foundation_array(return_array) == foundation_false);

	*output_array = *return_array;

	return return_code;
}

foundation_error_t parse_input_foundation_array(struct foundation_array_t *input_array, struct foundation_array_t *output_array) {
	foundation_error_t return_code = FOUNDATION_ERROR_SUCCESS;
	struct foundation_iterator_t *inputArrayIterator = new_iterator_foundation_array(input_array);
	struct foundation_object_t *currentInputArrayElement = inputArrayIterator->get_next(inputArrayIterator);

	struct foundation_array_t *wordsArray = new_foundation_array(0, (foundation_compare)foundation_string_comparison, free);

	char *currentWord = (char *)malloc(sizeof(char) * 0);
	assert(currentWord != NULL);
	int currentWordLength = 0;

	while (currentInputArrayElement) {

		if (*(char *)inputArrayIterator->get_value(currentInputArrayElement) == WORDS_DELIMITER || *(char *)inputArrayIterator->get_value(currentInputArrayElement) == LINE_FINISH) {
			currentWord = realloc(currentWord, sizeof(char) * ++currentWordLength);
			currentWord[currentWordLength - 1] = '\0';
			push_back_foundation_array(wordsArray, currentWord, (size_t)currentWordLength);

			assert(empty_foundation_array(wordsArray) == foundation_false);

			currentWord = 0;
			currentWordLength = 0;
		} else {

			currentWord = realloc(currentWord, sizeof(char) * ++currentWordLength);
			assert(currentWord != NULL);

			currentWord[currentWordLength - 1] = *(char *)inputArrayIterator->get_value(currentInputArrayElement);
		}

		currentInputArrayElement = inputArrayIterator->get_next(inputArrayIterator);
	}

	delete_iterator_foundation_array(inputArrayIterator);

	*output_array = *wordsArray;

	return return_code;
}

foundation_error_t check_input_correctness(struct foundation_array_t *input_array) {
	struct foundation_iterator_t *inputArrayIterator = new_iterator_foundation_array(input_array);
	struct foundation_object_t *currentInputArrayElement = inputArrayIterator->get_next(inputArrayIterator);

	while (currentInputArrayElement) {
		char *currentWord = inputArrayIterator->get_value(currentInputArrayElement);
		int currentUpperAmount = 0, currentWordIterator = 0;

		while (currentWordIterator != currentInputArrayElement->size - 1) {
			if (isupper(*currentWord)) {
				currentUpperAmount++;
			}

			currentWord++;
			currentWordIterator++;
		}

		if (currentUpperAmount < MIN_UPPER_LETTERS_COUNT || currentUpperAmount > MAX_UPPER_LETTERS_COUNT) {
			return FOUNDATION_ERROR_ERROR;
		}

		currentInputArrayElement = inputArrayIterator->get_next(inputArrayIterator);
	}

	return FOUNDATION_ERROR_SUCCESS;
}

foundation_error_t remove_duplicates_foundation_array(struct foundation_array_t *input_array) {
	if (input_array == NULL) {
		return FOUNDATION_ARRAY_NOT_INITIALIZED;
	}

	for (unsigned int iterator = 0; iterator < input_array->amount; iterator++) {
		for (unsigned int jterator = 0; jterator < input_array->amount; jterator++) {
			char **firstWord = (char **)malloc(sizeof(char *));
			char **secondWord = (char **)malloc(sizeof(char *));
			element_at_foundation_array(input_array, iterator, (void **)firstWord);
			element_at_foundation_array(input_array, jterator, (void **)secondWord);

			if (iterator == jterator) {
				continue;
			} else if (strcmp(*firstWord, *secondWord) == 0) {
				remove_from_foundation_array(input_array, jterator);
				jterator = 0;
			}

			free(firstWord);
			free(secondWord);
		}
	}

	return FOUNDATION_ERROR_SUCCESS;
}

int main(void) {
	struct foundation_array_t *inputArray = (struct foundation_array_t *)malloc(sizeof(struct foundation_array_t));
	if (get_input_foundation_array(inputArray)) {
		printf("\nInput error.\n");
		return FOUNDATION_INPUT_ERROR;
	}

	struct foundation_array_t *wordsArray = (struct foundation_array_t *)malloc(sizeof(struct foundation_array_t));

	if (parse_input_foundation_array(inputArray, wordsArray)) {
		printf("\nInput parsing error.\n");
		return FOUNDATION_INPUT_ERROR;
	}

	delete_foundation_array(inputArray);

	if (check_input_correctness(wordsArray)) {
		printf("\nInput string is not correct. Check input format!\n");
		return FOUNDATION_INPUT_FORMAT_ERROR;
	}

	qsort(wordsArray->elements, (size_t)wordsArray->amount, sizeof(char *), foundation_string_comparison);

	if (remove_duplicates_foundation_array(wordsArray)) {
		printf("Duplicate words remove process failed. Check the program code!");
		return FOUNDATION_ARRAY_INSERT_FAILED;
	}

	struct foundation_slist_t *outputList = new_foundation_slist((foundation_compare)foundation_string_comparison, free);

	struct foundation_iterator_t *wordsArrayIterator = new_iterator_foundation_array(wordsArray);
	struct foundation_object_t *currentWordsArrayElement = wordsArrayIterator->get_next(wordsArrayIterator);

	while (currentWordsArrayElement) {
		push_back_foundation_slist(outputList, wordsArrayIterator->get_value(currentWordsArrayElement), currentWordsArrayElement->size);

		currentWordsArrayElement = wordsArrayIterator->get_next(wordsArrayIterator);
	}

	delete_iterator_foundation_array(wordsArrayIterator);
	delete_foundation_array(wordsArray);

	struct foundation_iterator_t *listIterator = new_iterator_foundation_slist(outputList);
	struct foundation_object_t *currentListElement = listIterator->get_next(listIterator);

	while (currentListElement) {
		printf("\n%s", (char *)listIterator->get_value(currentListElement));

		currentListElement = listIterator->get_next(listIterator);
	}

	delete_iterator_foundation_slist(listIterator);

	return 0;
}