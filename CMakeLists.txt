cmake_minimum_required(VERSION 2.8.11 FATAL_ERROR)
set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS TRUE)

cmake_policy(SET CMP0026 NEW)
cmake_policy(SET CMP0042 NEW)

project(SecondTask)

set(FIND_QUIETLY FALSE)
set(DBG_FIND_QUIETLY TRUE)
set(FIND_EXTR_QUIETLY FALSE)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
	"${PROJECT_SOURCE_DIR}/CMake"
	"${PROJECT_SOURCE_DIR}/CMake/FindPackages"
)

set(CMAKE_INCLUDE_CURRENT_DIR TRUE)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/Build/${CMAKE_SYSTEM_NAME}/Products/${CMAKE_BUILD_TYPE}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/Build/${CMAKE_SYSTEM_NAME}/Products/${CMAKE_BUILD_TYPE}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/Build/${CMAKE_SYSTEM_NAME}/Products/${CMAKE_BUILD_TYPE}")

add_subdirectory("${PROJECT_SOURCE_DIR}/Sources")